# -*- encoding: utf-8 -*-
# stub: markable 0.1.4 ruby lib

Gem::Specification.new do |s|
  s.name = "markable"
  s.version = "0.1.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Alex Chrome"]
  s.date = "2016-11-25"
  s.description = "Markable allows you to easily create a marking system in your rails application."
  s.email = ["dify.chrome@gmail.com"]
  s.files = ["README.md", "Rakefile", "lib/generators/markable/migration/migration_generator.rb", "lib/generators/markable/migration/templates/active_record/migration.rb", "lib/markable.rb", "lib/markable/acts_as_markable.rb", "lib/markable/acts_as_marker.rb", "lib/markable/exceptions.rb", "lib/markable/version.rb", "lib/models/mark.rb", "lib/tasks/markable.rake", "test/acts_as_markable_test.rb", "test/acts_as_marker_test.rb", "test/dummy/README.rdoc", "test/dummy/Rakefile", "test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/helpers/application_helper.rb", "test/dummy/app/models/admin.rb", "test/dummy/app/models/car.rb", "test/dummy/app/models/city.rb", "test/dummy/app/models/drink.rb", "test/dummy/app/models/food.rb", "test/dummy/app/models/user.rb", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/config.ru", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments/development.rb", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/secret_token.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/locales/en.yml", "test/dummy/config/routes.rb", "test/dummy/db/migrate/20120214103734_create_users.rb", "test/dummy/db/migrate/20120214103741_create_admins.rb", "test/dummy/db/migrate/20120214105411_create_foods.rb", "test/dummy/db/migrate/20120214105421_create_drinks.rb", "test/dummy/db/migrate/20120214114957_markable_migration.rb", "test/dummy/db/migrate/20120520101223_create_cars.rb", "test/dummy/db/migrate/20120520101231_create_cities.rb", "test/dummy/db/schema.rb", "test/dummy/public/404.html", "test/dummy/public/422.html", "test/dummy/public/500.html", "test/dummy/public/favicon.ico", "test/dummy/script/rails", "test/dummy/test/fixtures/admins.yml", "test/dummy/test/fixtures/drinks.yml", "test/dummy/test/fixtures/foods.yml", "test/dummy/test/fixtures/users.yml", "test/dummy/test/unit/admin_test.rb", "test/dummy/test/unit/drink_test.rb", "test/dummy/test/unit/food_test.rb", "test/dummy/test/unit/user_test.rb", "test/mark_test.rb", "test/support/get.rb", "test/test_helper.rb"]
  s.homepage = "https://github.com/chrome/markable"
  s.rubygems_version = "2.4.8"
  s.summary = "Marking system for rails app"
  s.test_files = ["test/mark_test.rb", "test/acts_as_marker_test.rb", "test/acts_as_markable_test.rb", "test/support/get.rb", "test/test_helper.rb", "test/dummy/db/migrate/20120214105421_create_drinks.rb", "test/dummy/db/migrate/20120520101223_create_cars.rb", "test/dummy/db/migrate/20120214103734_create_users.rb", "test/dummy/db/migrate/20120214103741_create_admins.rb", "test/dummy/db/migrate/20120520101231_create_cities.rb", "test/dummy/db/migrate/20120214105411_create_foods.rb", "test/dummy/db/migrate/20120214114957_markable_migration.rb", "test/dummy/db/schema.rb", "test/dummy/script/rails", "test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/app/models/drink.rb", "test/dummy/app/models/food.rb", "test/dummy/app/models/user.rb", "test/dummy/app/models/car.rb", "test/dummy/app/models/admin.rb", "test/dummy/app/models/city.rb", "test/dummy/app/helpers/application_helper.rb", "test/dummy/Rakefile", "test/dummy/public/favicon.ico", "test/dummy/public/500.html", "test/dummy/public/422.html", "test/dummy/public/404.html", "test/dummy/config.ru", "test/dummy/README.rdoc", "test/dummy/test/unit/user_test.rb", "test/dummy/test/unit/drink_test.rb", "test/dummy/test/unit/admin_test.rb", "test/dummy/test/unit/food_test.rb", "test/dummy/test/fixtures/users.yml", "test/dummy/test/fixtures/drinks.yml", "test/dummy/test/fixtures/foods.yml", "test/dummy/test/fixtures/admins.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/environments/development.rb", "test/dummy/config/locales/en.yml", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/secret_token.rb", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/boot.rb", "test/dummy/config/routes.rb", "test/dummy/config/database.yml", "test/dummy/config/application.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 3.1"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
    else
      s.add_dependency(%q<rails>, [">= 3.1"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, [">= 3.1"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
  end
end
